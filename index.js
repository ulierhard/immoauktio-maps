const dotenv = require("dotenv").config();
const SshClient = require("ssh2").Client;
let SftpClient = require("ssh2-sftp-client");

// Connection Config

const config = {
    host: process.env.SSH_IP,
    port: process.env.SSHPORT,
    username: process.env.SSH_USER,
    password: process.env.SSH_PASS,
    privateKey: require("fs").readFileSync(process.env.SSH_PRIVATE_KEY_PATH)
};

// Upload everything
module.exports.uploadAll = function() {
    async function main() {
        const client = new SftpClient("upload");
        const src = "tileserver";
        const dst = "/root";

        try {
            await client.connect(config);
            client.on("upload", (info) => {
                console.log(`Listener: Uploaded ${info.source}`);
            });
            let rslt = await client.uploadDir(src, dst);
            return rslt;
        } finally {
            restartMapServer();
            client.end();
        }
    }
    main()
        .then((msg) => {
            console.log(msg);
        })
        .catch((err) => {
            console.log(`main error: ${err.message}`);
        });
};

// Upload Icons
module.exports.uploadSprites = function() {
    async function main() {
        const client = new SftpClient("upload");
        const src = "tileserver/sprites/output";
        const dst = "/root/sprites/immoauktio";

        try {
            await client.connect(config);
            client.on("upload", (info) => {
                console.log(`Listener: Uploaded ${info.source}`);
            });
            let rslt = await client.uploadDir(src, dst);
            return rslt;
        } finally {
            restartMapServer();
            client.end();
        }
    }
    main()
        .then((msg) => {
            console.log(msg);
        })
        .catch((err) => {
            console.log(`main error: ${err.message}`);
        });
};
// Upload style
module.exports.uploadStyle = function() {
    let client = new SftpClient();

    let data = fs.createReadStream("tileserver/styles/immoauktio-style.json");
    let remote = "/root/styles/immoauktio-style.json";

    client
        .connect(config)
        .then(() => {
            return client.put(data, remote);
        })
        .then(() => {
            console.log("Styles uploaded...");
            restartMapServer();
            return client.end();
        })
        .catch((err) => {
            console.error(err.message);
        });
};

// Upload style
module.exports.uploadConfig = function() {
    let client = new SftpClient();

    let data = fs.createReadStream("tileserver/mapserver-config.json");
    let remote = "/root/mapserver-config.json";

    client
        .connect(config)
        .then(() => {
            return client.put(data, remote);
        })
        .then(() => {
            console.log("Config uploaded...");
            restartMapServer();
            return client.end();
        })
        .catch((err) => {
            console.error(err.message);
        });
};

// Upload fonts
module.exports.uploadFonts = function() {
    async function main() {
        const client = new SftpClient("upload");
        const src = "tileserver/fonts";
        const dst = "/root/fonts";

        try {
            await client.connect(config);
            client.on("upload", (info) => {
                console.log(`Listener: Uploaded ${info.source}`);
            });
            let rslt = await client.uploadDir(src, dst);
            return rslt;
        } finally {
            restartMapServer();
            client.end();
        }
    }
    main()
        .then((msg) => {
            console.log(msg);
        })
        .catch((err) => {
            console.log(`main error: ${err.message}`);
        });
};

// Server start

function startMapServer() {
    var conn = new SshClient();
    conn.on("ready", function() {
        console.log("Client :: ready");
        conn.exec(
            // "docker run --name mapserver -v $(pwd):/data -p 8080:80 klokantech/tileserver-gl germany.mbtiles --verbose --config mapserver-config.json",
            "docker start mapserver",
            function(err, stream) {
                if (err) throw err;
                stream
                    .on("close", function(code, signal) {
                        console.log("Stream :: close :: code: " + code + ", signal: " + signal);
                        console.log("mapserver started");
                        conn.end();
                    })
                    .on("data", function(data) {
                        console.log("STDOUT: " + data);
                    })
                    .stderr.on("data", function(data) {
                        console.log("STDERR: " + data);
                    });
            }
        );
    }).connect(config);
}

module.exports.startMapServer = startMapServer;

// Server restart

function restartMapServer() {
    var conn = new SshClient();
    conn.on("ready", function() {
        console.log("Client :: ready");
        conn.exec("docker restart mapserver", function(err, stream) {
            if (err) throw err;
            stream
                .on("close", function(code, signal) {
                    console.log("Stream :: close :: code: " + code + ", signal: " + signal);
                    console.log("mapserver restarted");
                    conn.end();
                })
                .on("data", function(data) {
                    console.log("STDOUT: " + data);
                })
                .stderr.on("data", function(data) {
                    console.log("STDERR: " + data);
                });
        });
    }).connect(config);
}

module.exports.restartMapServer = restartMapServer;

// Server stop

function stopMapServer() {
    var conn = new SshClient();
    conn.on("ready", function() {
        console.log("Client :: ready");
        conn.exec("docker stop mapserver", function(err, stream) {
            if (err) throw err;
            stream
                .on("close", function(code, signal) {
                    console.log("Stream :: close :: code: " + code + ", signal: " + signal);
                    console.log("mapserver stopped");
                    conn.end();
                })
                .on("data", function(data) {
                    console.log("STDOUT: " + data);
                })
                .stderr.on("data", function(data) {
                    console.log("STDERR: " + data);
                });
        });
    }).connect(config);
}

module.exports.stopMapServer = stopMapServer;
