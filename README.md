# immoauktio-maps

**URL**  
[https://maps.immoauktio.de](https://maps.immoauktio.de)

| command                      | description                                                                              |
| ---------------------------- | ---------------------------------------------------------------------------------------- |
| `npm run map-dev`            | Start local development <br> maputnik runs under [localhost:8000](http://localhost:8000) |
| `npm run start-mapserver`    | Start remote map server                                                                  |
| `npm run restart-mapserver`  | Restart remote map server                                                                |
| `npm run stop-mapserver`     | Stop remote map server                                                                   |
| `npm run upload-all`         | Upload config, style, fonts to map server & restart map server                           |
| `npm run upload-style`       | Upload style only to map server & restart map server                                     |
| `npm run upload-config`      | Upload config only to map server & restart map server                                    |
| `npm run upload-uploadFonts` | Upload fonts only to map server & restart map server                                     |

## Documentations

**Open Map Tiles**  
[https://github.com/openmaptiles](https://github.com/openmaptiles)  
[https://openmaptiles.org/schema/](https://openmaptiles.org/schema/)

**Maputnik Editor**  
[https://github.com/maputnik/editor](https://github.com/maputnik/editor)

**Open Tile Server GL**  
[https://openmaptiles.org/docs/host/tileserver-gl/](https://openmaptiles.org/docs/host/tileserver-gl/)  
[https://tileserver.readthedocs.io/en/latest/](https://tileserver.readthedocs.io/en/latest/)

**Sprite Zero** _(creating map icons)_  
[https://github.com/mapbox/spritezero](https://github.com/mapbox/spritezero)  
[https://github.com/mapbox/spritezero-cli](https://github.com/mapbox/spritezero-cli)

for use switch to node version 8.9.4 (command `n 8.9.4`)

---

**Other stuff**  
restart map server docker command  
`docker run -it -v $(pwd):/data -p 8080:80 klokantech/tileserver-gl germany.mbtiles --verbose --config mapserver-config.json`
